Download a file with powershell
===============================

2 lines in PowerShell will let you download a file from a remote web source.

Code: 
	$client = new-object System.Net.WebClient
	client.DownloadFile("Source URL","Destination File")

Example: 
	$client = new-object System.Net.WebClient
	$client.DownloadFile("http://i.imgur.com/JnphmRt.jpg","C:\cat.jpg")