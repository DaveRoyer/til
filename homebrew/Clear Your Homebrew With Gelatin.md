Clear Your Homebrew With Gelatin
================================

# Supplies
* 1 tsp gelatin 
* 2/3 cups cold water

# Procedure

- Stir the gelatin into the cold water until disolved.
- Microwave the mixture 15-30 seconds at a time, stirring after each time period. Measure the temerature often. THe goal is to heat it to 150 degrees Farenheight. If it reaches 155, that's ok but we don't want to get over 170.
- Give the mixture one last stir and dump it in the beer. 
- Gently swirl the fermenter or keg, and return it to your fridge or kegerator for 24-48 hours.
- If you used a keg, purge the headspace with CO2 to remove any oxygen that got mixed in.

These notes were taken from the blog [Bertus Brewery][http://www.bertusbrewery.com/2012/06/how-to-clear-your-beer-with-gelatin.html?m=1].