Understanding VLAN's
====================

From [this reddit conversation](https://www.reddit.com/r/networking/comments/485wet/understanding_vlans_or_not/) comes a discussion about understanding VLANs.

> Think of a network as a roadway system.

> * Access/Untagged ports are single lane onramps and offramps.
> * Intersections (where the onramps, offramps and highway meet) are switches.
> * Tagged/Trunk ports are multi-lane highways with concrete barriers dividing the lanes that connect intersections.

> If you drive up an onramp (access/untagged port), you get put into a specific lane (VLAN) of a multi-lane road (tagged/trunk port). The concrete barriers between the lanes stop you from merging into other lanes, although all lanes are on the same stretch of road going between the same 2 intersections. In order to get to your desination, the intersection has to have an offramp (access/untagged port) in your lane, otherwise you can do nothing other than keep on driving.

> But what if your lane doesn't go to where you want to end up? Afterall, you can't change lanes because of the concrete barriers. You get on at your usual onramp and drive towards a major intersection (router) that lets you take the offramp, change lanes and then take an onramp into another lane that takes you to the next intersection (switch) where there is an offramp (access/untagged port) to let you off. Sometimes, this means driving past where you want to get off, so you can changes lanes at the major intersection (router) and then head back to where you want to go.