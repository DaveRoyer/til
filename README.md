# Today I Learned

We constantly learn new things. This is a repo to share those learnings.
TILs are short Markdown documents (a few sentences + example code) explaining
concepts, bits of syntax, commands, or tips we've recently learned.

For new TILs, watch this repo or follow [@daveroyer] on Twitter.

[@daveroyer]: https://twitter.com/upcase

License
-------

© 2015 Dave Royer
Distributed under the [Creative Commons Attribution License][license].

Original idea from [thoughtbot/til] on github.

[thoughtbot/til]: https://github.com/thoughtbot/til
[license]: http://creativecommons.org/licenses/by/3.0/

Names and logos for thoughtbot are trademarks of thoughtbot, inc.